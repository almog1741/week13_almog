#include "Server.h"
#include "Helper.h"
#include <exception>
#include <iostream>
#include <string>
#include <string.h>
#include <thread>
#include <vector>
#include <fstream>
#include <queue>
#define MAX_MSG_LEN 1000
#define SERVER_UPDATE_MSG_CODE 101
#define CLIENT_UPDATE_MSG_CODE 204 


Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	//activate a thread that print the messages to the conversation files
	std::thread send_messages_thread(&Server::send_messages_to_file, this);
	send_messages_thread.detach();


	//accepting clients
	while (true)
	{
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	
	std::string msg_str = "";
	std::string username = "";
	char msg_array[MAX_MSG_LEN] = "";
	int username_len = 0;

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	//remove the msg type code from the given msg
	//i know it is 200 becuase the client app when activated only allow to connect and not to send messages 
	//and it is not sending update request before it has connected to the server
	Helper::getMessageTypeCode(client_socket);

	//get the username length from the msg
	username_len = Helper::getIntPartFromSocket(client_socket, 2);

	//get the username from the msg
	username = Helper::getStringPartFromSocket(client_socket, username_len);

	//add the user to the connected users
	this->_connected_users_mutex.lock();
	//check if this username is not already taken 
	if (this->_connected_users.find(username) == this->_connected_users.end())
	{
		this->_connected_users.insert({ username, client_socket });
	}
	else
	{
		std::cout <<std::endl << "can't have 2 users with the same name" << std::endl;
		exit(1);
	}
	this->_connected_users_mutex.unlock();

	//send to the user msg that the connection succeeded and the current connected users
	Helper::send_update_message_to_client(client_socket, "", "", this->get_connected_users().c_str());

	//create a thread that will handle the user requests
	std::thread client_handler_thread(&Server::clientHandler, this ,client_socket, username);

	client_handler_thread.detach();
}


void Server::clientHandler(SOCKET clientSocket, std::string username)
{
	std::vector<std::string> msg_data;
	std::string msg_to_receiver = "";
	std::string receiver_name = "";
	int receiver_name_len = 0;
	int msg_to_receiver_len = 0;
	std::string protocol_msg = "";
	std::ifstream file;
	std::string filename;
	std::string file_buffer;
	std::string line;

	while (true)
	{
		try 
		{
			//remove the msg type code from the given msg
			//there is only one code type available is 204
			Helper::getMessageTypeCode(clientSocket);

			//get the message info
			receiver_name_len = Helper::getIntPartFromSocket(clientSocket, 2);
			receiver_name = Helper::getStringPartFromSocket(clientSocket, receiver_name_len);
			msg_to_receiver_len = Helper::getIntPartFromSocket(clientSocket, 5);
			msg_to_receiver = Helper::getStringPartFromSocket(clientSocket, msg_to_receiver_len);

			//if there is no message then only update the user with the connected users
			if (msg_to_receiver_len == 0)
			{
				//if there is no message then the recevier is the person the user is taking to
				if (receiver_name_len != 0)
				{
					//get the filename
					filename = this->get_filename(username, receiver_name);

					//open the file
					file.open(filename, std::ios::in);
					if (file.is_open())
					{
						//get the buffer
						while (std::getline(file, line))
						{
							file_buffer += line;
						}

						file.close();
						//send the buffer
						Helper::send_update_message_to_client(clientSocket, file_buffer, receiver_name, this->get_connected_users());
						//reset the buffer
						file_buffer = "";
					}
					//if the file doesn't exsist (can't open)
					else
					{
						Helper::send_update_message_to_client(clientSocket, "", "", this->get_connected_users());
					}
				}
				//if there is no recevier (somone in the chat of the user) then update hoim with the connected users
				else
				{
					Helper::send_update_message_to_client(clientSocket, "", "", this->get_connected_users());
				}
			
				
			}
			else //if the user sent a msg for another user then send it and save to the conversation file of the user and the receiver
			{
				//create the msg by the protocol
				protocol_msg = "&MAGSH_MESSAGE&&Author&" + username + "&DATA&" + msg_to_receiver;

				//put the msg info into a vector
				msg_data.push_back(receiver_name);
				msg_data.push_back(protocol_msg);
				msg_data.push_back(username);

				//lock the shared messages queue then add the vector with the data in 
				this->_messages_mutex.lock();
				this->_messages.push(msg_data);
				this->_messages_mutex.unlock();
				this->_messages_cv.notify_all();
				//reset the vector for the next loop
				msg_data.clear();
			}
		}
		catch (const std::exception & e)
		{
			//close the socket and remove the user from the connected users
			closesocket(clientSocket);
			//lock th mutex of the connectred users
			this->_connected_users_mutex.lock();
			this->_connected_users.erase(this->_connected_users.find(username));
			this->_connected_users_mutex.unlock();
			break; 
		}
	}
}


std::string Server::get_connected_users()
{
	std::string connected_users = "";

	this->_connected_users_mutex.lock();

	//go over all the users and return them to the caller
	std::map <std::string, SOCKET>::iterator it = this->_connected_users.begin();
	while (it != this->_connected_users.end())
	{
		if (connected_users != "")
		{
			connected_users += '&';
		}
		connected_users += it->first;
		it++;

	}
	this->_connected_users_mutex.unlock();
	return connected_users;
}


void Server::send_messages_to_file()
{
	std::vector<std::string> msg_info;
	std::ofstream file;
	std::string filename;

	while (true)
	{
		std::unique_lock<std::mutex> unique_lock(this->_messages_mutex);
		//wait till informed that there is a msg to send
		this->_messages_cv.wait(unique_lock);

		//no need to check if the queue is not empty because the other thread informed that he added a msg
		msg_info = this->_messages.front();
		this->_messages.pop();
		unique_lock.unlock();

		//get the filename
		filename = this->get_filename(msg_info[2], msg_info[0]);
		
		file.open(filename, std::ios_base::app);
		file << msg_info[1];
		file.close();

		msg_info.clear();
	}
}


std::string Server::get_filename(std::string name1, std::string name2)
{
	if (name1.compare(name2) < 0)
	{
		return  name1 + '&' + name2 + ".txt";
	}
	else if (name1.compare(name2) > 0)
	{
		return  name2 + '&' + name1 + ".txt";
	}
	else
	{
		return  name1 + '&' + name2 + ".txt";
	}
}
