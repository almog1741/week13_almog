#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <mutex>
#include <utility>

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:

	//functions
	void accept();
	void clientHandler(SOCKET clientSocket, std::string username);
	std::string get_connected_users();
	void send_messages_to_file();
	std::string get_filename(std::string name1, std::string name2);

	//variables
	SOCKET _serverSocket;
	//saves the name and the socket of the connected for users to comunicate
	std::map <std::string , SOCKET> _connected_users;
	//saves the messages
	std::queue <std::vector <std::string>> _messages;
	std::mutex _messages_mutex;
	std::mutex _connected_users_mutex;
	std::condition_variable _messages_cv;
};

